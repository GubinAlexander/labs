import argparse
from src import Manager, Project, Task
from src.consts import TASK_DEFAULT_STATUS, TASK_DEFAULT_PRIORITY, TASK_DEFAULT_DEVELOPER

def login(args):
    user_name = args.user_name
    password = args.password
    try:
        Manager.authorize(user_name, password)
        print('Authorized successfully')
    except Exception as e:
        print(e)

def register(args):
    user_name = args.user_name
    name = args.name
    password = args.password
    try:
        Manager.register_developer(user_name, name, password)
        print('Registered successfully')
    except Exception as e:
            print(e)

def add_task(args):
    name  = args.name
    description = args.description
    status = args.status
    priority = args.priority
    assignee = args.assignee
    subject = args.subject
    dead_line = args.dead_line
    period = args.period
    try:
        Manager.add_task(name, description, status, priority, assignee, subject, dead_line, period)
        print('Added successfully')
    except Exception as e:
        print('There is no such value in the data base {0}'.format(e))

def change_task(args):
    task_id = args.task_id
    name = args.name
    subject = args.subject
    description = args.description
    priority = args.priority
    assignee = args.assignee
    dead_line = args.dead_line
    period = args.period
    try:
        Manager.change_task(task_id, name, subject, description, priority, assignee, dead_line, period)
        print('Changed successfully')
    except Exception as e:
        print('There is no such value in the data base {0}'.format(e))

def remove_task(args):
    task_id = args.task_id
    try:
        Manager.remove_task(task_id)
        print('Removed successfully')
    except Exception as e:
        print(e)

def get_task_list(args):
    status = 'All'
    if args.to_do:
        status = 'To do'
    elif args.in_progress:
        status = 'In progress'
    elif args.resolved:
        status = 'Resolved'
    try:
        for task in Manager.get_task_list(status):
            print(task)

    except AttributeError as e:
        print(e)

def resolve_task(args):
    id = args.id
    try:
        Manager.resolve_task(id)
        print('Task resolved successfully')
    except Exception as e:
        print(e)

def show_current_developers(args=None):
    print(Manager.current_dev)

def show_all_developers(args=None):
    for dev in Manager.developers:
        print(Manager.developers[dev])

def add_project(args):
    name = args.name
    Manager.add_project(name)
    print('Added project successfully')

def add_project_task(args):
    name = args.name
    description = args.description
    status = args.status
    priority = args.priority
    assignee = args.assignee
    subject = args.subject
    dead_line = args.dead_line
    period = args.period
    try:
        Manager.add_project_task(name, description, status, priority, assignee, subject, dead_line, period)
    except Exception as e:
        print(e)

def add_project_dev(args):
    user_name = args.user_name
    try:
        Manager.add_project_dev(user_name)
    except Exception as e:
        print(e)

def get_project_list(args):
    id = args.id
    type = args.type
    try:
        Manager.get_project_task_list(type, id)
    except Exception as e:
        print(e)

def parse_args():
    parser = argparse.ArgumentParser(description='Great argument parser for Ad_como')

    subparsers = parser.add_subparsers(help='sub-command help')

    developer_parser = subparsers.add_parser('developer', help='Log in or watch users')

    developer_subparsers = developer_parser.add_subparsers(help='sub-command help')

    login_parser = developer_subparsers.add_parser('login', help='Log in or watch developers')
    login_parser.add_argument('-u', '--user_name', help='Your login', default='Alex')
    login_parser.add_argument('-p', '--password', help='Your password', default='123')
    login_parser.set_defaults(func=login)

    register_parser = developer_subparsers.add_parser('register', help='Register in the project')
    register_parser.add_argument('-u', '--user_name', help='Your login', default='Alex', required=True)
    register_parser.add_argument('-n', '--name', help='Your name', default='Alex', required=True)
    register_parser.add_argument('-p', '--password', help='Your password', default='123', required=True)
    register_parser.set_defaults(func=register)

    cur_list_parser = developer_subparsers.add_parser('all', help='Display all developers')
    cur_list_parser.set_defaults(func=show_all_developers)

    all_list_parser = developer_subparsers.add_parser('current', help='Display current developers')
    all_list_parser.set_defaults(func=show_current_developers)

    add_parser = subparsers.add_parser('add', help='Add task in the project')
    add_parser.add_argument('-n', '--name', help='Name of the task', required=True)
    add_parser.add_argument('-d', '--description', help='Description of the task', default='Simple description')
    add_parser.add_argument('-s', '--status', help='Status of the task', default=TASK_DEFAULT_STATUS)
    add_parser.add_argument('-p', '--priority', help='Priority of the task', default=TASK_DEFAULT_PRIORITY)
    add_parser.add_argument('-a', '--assignee', help='Assignee to', default=TASK_DEFAULT_DEVELOPER)
    add_parser.add_argument('-sub', '--subject', help='Subject of the task', default='Simple subject')
    add_parser.add_argument('-dl', '--dead_line', help='Dead line of the task', default=None)
    add_parser.add_argument('-pr', '--period', help='Period of the task', default=None)
    add_parser.set_defaults(func=add_task)

    change_parser = subparsers.add_parser('change', help='Change task in the project')
    change_parser.add_argument('-id', '--task_id', help='Id task would you change', required=True)
    change_parser.add_argument('-n', '--name', help='Change name of the task')
    change_parser.add_argument('-s', '--subject', help='Change subject of the task')
    change_parser.add_argument('-des', '--description', help='Change description of the task')
    change_parser.add_argument('-p', '--priority', help='Change priority if the task')
    change_parser.add_argument('-a', '--assignee', help='Change assignee of the task')
    change_parser.add_argument('-d', '--dead_line', help='Change deadline of the task', default=None)
    change_parser.add_argument('-pr', '--period', help='Change period of the task', default=None)
    change_parser.set_defaults(func=change_task)

    task_list_parser = subparsers.add_parser('list', help='View all tasks in the project')
    view_list_parser = task_list_parser.add_mutually_exclusive_group()
    view_list_parser.add_argument('-r', '--resolved', action='store_true', help='View all resolved tasks')
    view_list_parser.add_argument('-t', '--to_do', action='store_true', help='View all to do tasks')
    view_list_parser.add_argument('-a', '--all', action='store_true', help='View all tasks')
    view_list_parser.add_argument('-i', '--in_progress', action='store_true', help='View all in progress tasks')
    task_list_parser.set_defaults(func=get_task_list)

    resolved_parser = subparsers.add_parser('resolve', help='Which task would you like to resolve')
    resolved_parser.add_argument('-i', '--id', help='Task id to resolve', required=True)
    resolved_parser.set_defaults(func=resolve_task)

    remove_parser = subparsers.add_parser('remove', help='Choose which task would you remove')
    remove_parser.add_argument('-i', '--task_id', help='Task id to remove')
    remove_parser.set_defaults(func=remove_task)

    project_parser = subparsers.add_parser('project', help='Manage project')

    project_subparsers = project_parser.add_subparsers(help='sub-command help')

    add_project_parser = project_subparsers.add_parser('create', help='Create new project')
    add_project_parser.add_argument('-n', '--name', help='Name for your new project', required=True)
    add_project_parser.set_defaults(func=add_project)

    add_project_task = project_subparsers.add_parser('add_task', help='Add tasks in the project')
    add_project_task.add_argument('-n', '--name', help='Name of the task', required=True)
    add_project_task.add_argument('-d', '--description', help='Description of the task', default='Simple description')
    add_project_task.add_argument('-s', '--status', help='Status of the task', default=TASK_DEFAULT_STATUS)
    add_project_task.add_argument('-p', '--priority', help='Priority of the task', default=TASK_DEFAULT_PRIORITY)
    add_project_task.add_argument('-a', '--assignee', help='Assignee to', default=TASK_DEFAULT_DEVELOPER)
    add_project_task.add_argument('-sub', '--subject', help='Subject of the task', default='Simple subject')
    add_project_task.add_argument('-dl', '--dead_line', help='Dead line of the task', default=None)
    add_project_task.add_argument('-pr', '--period', help='Period of the task', default=None)
    add_project_task.set_defaults(func=add_project_task)

    add_project_dev = project_subparsers.add_parser('add_dev', help='Add dev in the project')
    add_project_dev.add_argument('-u', '--user_name', help='Add user name of dev you want to add', required=True)
    add_project_dev.set_defaults(func=add_project_dev)

    get_list_project = project_subparsers.add_parser('list', help='Get list task of project')
    get_list_project.add_argument('-i', '--id', help='Id which project you would like to get', required=True)
    get_list_project.add_argument('-t', '-type', help='Type of list you would like', required=True)
    get_list_project.set_defaults(func=get_project_list)


    args = parser.parse_args()
    if 'func' in args:
        args.func(args)


def main():
    try:
        Manager.run()
    except FileNotFoundError as e:
        print(e)
    parse_args()

    add_project_task()
    Manager.save_developers()
    Manager.save_project()
if __name__ == '__main__':
    main()