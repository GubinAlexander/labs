class TaskList():
    def __init__(self, tasks=None, name='Task list'):
        self.name = name
        self.tasks = {}
        if tasks:
            if isinstance(tasks, list):
                for task in tasks:
                    self.tasks[task.id] = task
            else:
                self.tasks[tasks.id] = tasks
        self.root_tasks = [task_id for task_id in self.tasks]

    def __str__(self):
        s = []
        for task in self.tasks.values():
            s.append(task)
        return '\n'.join(s)

    def add_task(self, task):
        self.tasks[task.id] = task

    def get_list(self):
        s = []
        for value in self.tasks.values():
            s.append(value)
        return s

    def sort_by(self):
        self.tasks.sort(key=lambda task: task.priority)

    def get_task(self, task_id):
        if task_id not in self.tasks:
            raise Exception('There is no task with that id')
        else:
            return self.tasks.get(task_id, None)

    def remove_task(self, task_id):
        if task_id not in self.tasks:
            raise Exception ('Task with that id does not exists')
        else:
            del self.tasks[task_id]
