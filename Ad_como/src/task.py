import hashlib

from dateutil.relativedelta import relativedelta
from datetime import datetime
from datetime import timedelta
from .consts import TASK_DEFAULT_STATUS, TASK_DEFAULT_PRIORITY, TASK_DEFAULT_DEVELOPER, \
    REPEAT, STATUSES, PRIORITIES, DEVELOPERS, TIME_REPEAT

def increment_id_sub_task(method):
    '''Decorator for subtasks' id.'''
    def _wrapper(self, *args, **kwargs):
        self.sub_task_id += 1
        method(self, *args, **kwargs)
    return _wrapper

class Task():
    def __init__(self, name='Test user story', description='Description of the task', status=TASK_DEFAULT_STATUS,
                 priority=TASK_DEFAULT_PRIORITY, assignee=TASK_DEFAULT_DEVELOPER, subject='Subject', dead_line=None,
                 period=None):
        self.name = name
        self.date = datetime.now().strftime('%d/%m/%Y')
        self.dead_line = dead_line
        if dead_line:
            self.check_dead_line(dead_line)
        self.period = period
        if period:
            self.repeat_task(period)
        self.status = STATUSES[status]
        self.priority = PRIORITIES[priority]
        self.assignee = DEVELOPERS[assignee]
        self.description = description
        self.subject = subject
        self.subtasks = {}
        self.id = 0
        self.mod_time = None
        self.sub_task_id = 0
        self.id = hashlib.sha224(bytes(str(self), 'utf-8')).hexdigest()[:10]

    def __str__(self):
        subtask = []
        for value in self.subtasks.values():
            subtask.append(str(value))

        return '{0} #{1}\nStatus: {2}\nPriority: {3}\nAssignee: {4}\nStart date: {5}\nDue date: {6}\nDescription: ' \
               '{7}\nSubject: {8}\n'.format(self.name, self.id, self.status, self.priority, self.assignee, self.date,
                                            self.dead_line, self.description, self.subject)

    @increment_id_sub_task
    def add_subtask(self, sub_task):
        if sub_task not in self.subtasks:
            self.subtasks[self.sub_task_id] = sub_task
        else:
            raise Exception('Subtask has already exist')

    def remove_subtask(self, sub_task_id):
        if sub_task_id not in self.subtasks:
            raise AttributeError('There is no such key in dict')
        else:
            del self.subtasks[sub_task_id]

    def change_task(self, name=None, subject=None, description=None, priority=None, assignee=None, dead_line=None,
                    period=None):
        if name:
            self.name = name
        if subject:
            self.subject = subject.strip()
        if description:
            self.description = description.strip()
        if priority:
            self.priority = PRIORITIES[priority]
        if assignee:
            self.assignee = DEVELOPERS[assignee]
        if dead_line:
            self.check_dead_line(dead_line)
        if period:
            self.repeat_task(period)
        self.mod_time = datetime.now()

    def check_dead_line(self, dead_line):
        dead_line = datetime.strptime(dead_line, '%d.%m.%Y')
        if dead_line > datetime.now():
            self.dead_line = dead_line.strftime('%d/%m/%Y')
        else:
            raise AttributeError('Dead line cant be less than current time')

    def repeat_task(self, period):
        assert period in TIME_REPEAT, 'There is no such period'
        if self.dead_line:
            if period == 'Day':
                start = relativedelta(days=+1)
            elif period == 'Week':
                start = relativedelta(weeks=+1)
            elif period == 'Month':
                start = relativedelta(months=+1)
        else:
            raise ValueError('No dead line selected')
        self.period = start