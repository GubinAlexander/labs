import logging

from src.consts import DEFAULT_LOG_FILE_NAME

def _get_logger():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.setLevel(logging.ERROR)
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(DEFAULT_LOG_FILE_NAME)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger
