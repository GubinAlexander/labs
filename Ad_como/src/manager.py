import os
import jsonpickle
import pathlib
import datetime

from src import Task, Project, ProjectTask, Developer
from src.logger_tools import _get_logger

class Manager():
    def __init__(self, database_folder=None,
                 users_file=None, cur_user_file=None, projects_folder=None, log_file=None):
        self.developers = {}
        self.project = None
        self.current_dev = None
        self.logger = _get_logger()
        default = None

        if database_folder:
            self.database_folder = database_folder
        elif default:
            self.database_folder = default['database_folder']
        else:
            self.database_folder = 'data'

        if projects_folder:
            self.projects_folder = projects_folder
        elif default:
            self.projects_folder = default['projects_folder']
        else:
            self.projects_folder = 'projects'

        if users_file:
            self.users_file = users_file
        elif default:
            self.users_file = default['users_file']
        else:
            self.users_file = 'developers.json'

        if cur_user_file:
            self.current_dev_file = cur_user_file
        elif default:
            self.current_dev_file = default['current_dev_file']
        else:
            self.current_dev_file = 'current_dev.txt'

        if log_file:
            self.log_file = log_file
        elif default:
            self.log_file = default['log_file']
        else:
            self.log_file = 'log.log'
        try:
            self.load_users()
            self.load_project('last_project')
        except FileNotFoundError:
            pass

    def authorize(self, login, password):
        if login in self.developers:
            if self.developers[login].check_password(password):
                self.current_dev = self.developers[login]
                self.logger.info('Authorized as Developer {Developer}'.format(Developer=self.current_dev))
            else:
                self.logger.error('Wrong password for Developer {Developer}'.format(Developer=login))
                raise KeyError('Wrong password')
        else:
            self.logger.error('Developer {Developer} does not exist'.format(Developer=login))
            raise KeyError('Developer does not exist')

    def register_dev(self, name, login, password):
        if self.developers.get(login, None):
            self.logger.error('Developer {Developer} already exists'.format(Developer=login))
            raise KeyError('Developer with this login already exists')
        new_user = Developer(login, password, name)
        self.developers[login] = new_user
        self.current_dev = new_user
        self.logger.info('Developer {Developer} registered'.format(Developer=self.current_dev))

    def save_developers(self):
        if not os.path.exists(self.database_folder):
            os.mkdir(self.database_folder)
        with open(os.path.join(self.database_folder, self.users_file), 'w+') as f:
            f.write(jsonpickle.encode(self.developers))
        with open(os.path.join(self.database_folder, self.current_dev_file), 'w', encoding='utf-8') as f:
            if self.current_dev:
                f.write(self.current_dev.login)
        self.logger.debug('developers saved')

    def load_users(self):
        if not os.path.exists(self.database_folder):
            os.mkdir(self.database_folder)
            self.developers = None
        elif os.path.exists(os.path.join(self.database_folder, self.users_file)):
            with open(os.path.join(self.database_folder, self.users_file), 'r') as f:
                self.developers = jsonpickle.decode(f.readline())
        self.load_cur_user()
        self.logger.debug('developers loaded')

    def load_project(self, project_id):
        folder = os.path.join(self.database_folder, self.projects_folder)
        if not os.path.exists(folder):
            pathlib.Path(folder).mkdir(parents=True, exist_ok=True)
            self.project = None
            self.logger.error('No project with id #{id}'.format(id=project_id))
            raise FileNotFoundError('No project with current id')
        elif os.path.exists(os.path.join(folder, str(project_id) + '.json')):
            with open(os.path.join(folder, str(project_id) + '.json'), 'r') as f:
                try:
                    project = jsonpickle.decode(f.readline())
                    self.project = project
                    self.logger.debug('Project #{id} loaded'.format(id=project_id))
                except Exception as e:
                    self.project = None
                    self.logger.error('Bad project file')
                    raise e
        else:
            self.logger.error('No project with id #{id}'.format(id=project_id))
            raise FileNotFoundError('No project with current id')

    def save_project(self):
        folder = os.path.join(self.database_folder, self.projects_folder)
        if not os.path.exists(folder):
            pathlib.Path(folder).mkdir(parents=True, exist_ok=True)
        if self.project:
            with open(os.path.join(folder, str(self.project.id) + '.json'), 'w+') as f1, \
                    open(os.path.join(folder, 'last_project.json'), 'w+') as f2:
                try:
                    f1.write(jsonpickle.encode(self.project))
                    f2.write(jsonpickle.encode(self.project))
                    self.logger.debug('Project #{id} saved'.format(id=self.project.id))
                except Exception as e:
                    self.project = None
                    self.logger.error('Bad file, project #{id} cannot be saved'.format(id=self.project.id))
                    raise e

    def load_cur_user(self):
        if not os.path.exists(os.path.join(self.database_folder, self.current_dev_file)):
            open(os.path.join(self.database_folder, self.current_dev_file), 'w').close()
            self.current_dev = None
        else:
            with open(os.path.join(self.database_folder, self.current_dev_file), 'r', encoding='utf-8') as f:
                try:
                    login = f.readline().strip()
                    self.current_dev = self.developers[login]
                except KeyError:
                    self.current_dev = None
                    self.logger.error('No Developer with login #{id}'.format(id=login))

    def add_task(self, name, description, priority=0, parent_id=0, deadline=None, period=None):
        if not self.current_dev:
            self.logger.error('Cannot add task: there is no authorized Developer.')
            raise AttributeError('You are not logged in')
        task = Task(name=name, description=description, period=period,
                    parent_id=parent_id, priority=priority, end_date=deadline)
        self.current_dev.add_task(task)
        self.logger.info('Added task #{id}'.format(id=task.id))

    def change_task(self, task_id, name, description,
                  priority=0, deadline=None, period=None):
        if not self.current_dev:
            self.logger.error('Cannot edit task: there is no authorized Developer.')
            raise AttributeError('You are not logged in')
        self.current_dev.change_task(task_id=task_id, name=name, description=description,
                                priority=priority, deadline=deadline, period=period)
        self.logger.info('Edited task #{id}'.format(id=task_id))

    def get_full_task_info(self, task_id):
        try:
            s = self.current_dev.get_full_task_info(task_id)
            self.logger.info('Got full info of task #{id}'.format(id=task_id))
        except KeyError as e:
            self.logger.error('Task with id #{id} does not exist'.format(id=task_id))
            raise e
        return s

    def get_full_project_task_info(self, task_id, project_id):
        try:
            if project_id:
                self.load_project(project_id)
            s = self.project.get_full_task_info(task_id)
            self.logger.info('Got full info of task #{id}'.format(id=task_id))
        except KeyError as e:
            self.logger.error('Task with id #{id} does not exist'.format(id=task_id))
            raise e
        return s

    def add_project(self, project_name):
        self.project = Project(project_name)
        self.logger.info('Added project #{id}'.format(id=self.project.id))

    def add_project_task(self, name, description, parent_id=0, project_id=0,
                         priority=0, deadline=None, period=None):
        if not project_id and not self.project:
            self.logger.error('Cannot add task: there is no loaded project.')
            raise KeyError('Project is not loaded')
        if project_id:
            self.load_project(project_id)
        if self.project:
            task = ProjectTask(name=name, description=description,
                               parent_id=parent_id, created_user=self.current_dev.login,
                               priority=priority, end_date=deadline, period=period)
            self.project.add_task(task, self.current_dev.login)
            self.current_dev.projects.add(project_id)

    def edit_project_task(self, task_id, name, description,
                          project_id=0, priority=0, deadline=None, period=None):
        if project_id:
            self.load_project(project_id)
        self.project.change_task(task_id=task_id, name=name, description=description,
                               priority=priority, deadline=deadline, period=period)

    def get_task_list(self, list_type='pending'):
        if not self.current_dev:
            self.logger.error('Cannot get task list: there is no authorized Developer.')
            raise AttributeError('You are not logged in')
        s = None
        if list_type == 'pending':
            s = self.current_dev.pending_tasks.print_list()
        elif list_type == 'resolved':
            s = self.current_dev.completed_tasks.print_list()
        if s:
            self.logger.info('Got {type} task list'.format(type=list_type))
        return s

    def get_project_task_list(self, list_type='pending', project_id=0):
        if project_id:
            self.load_project(project_id)
        if not self.project:
            self.logger.error('Cannot get task list: there is no loaded project.')
            raise AttributeError('Cannot get task list: there is no loaded project.')
        if list_type == 'pending':
            return self.project.pending_tasks.print_list()
        elif list_type == 'resolved':
            return self.project.completed_tasks.print_list()

    def resolve_task(self, task_id):
        if not self.current_dev:
            self.logger.error('Cannot resolve task: there is no authorized Developer.')
            raise AttributeError('You are not logged in')
        self.current_dev.resolve_task(task_id)
        self.logger.info('Task #{id} resolved'.format(id=task_id))

    def move_task(self, source_id, destination_id):
        if not self.current_dev:
            self.logger.error('Cannot move task: there is no authorized Developer.')
            raise AttributeError('You are not logged in')
        self.current_dev.move_task(source_id, destination_id)
        self.logger.info('Task #{source_id} moved to sub tasks of #{dest}'.format(source_id=source_id,
                                                                                  dest=destination_id))

    def remove_task(self, task_id):
        if not self.current_dev:
            self.logger.error('Cannot remove task: there is no authorized Developer.')
            raise AttributeError('You are not logged in')
        self.current_dev.remove_task(task_id)
        self.logger.info('Task #{id} removed'.format(id=task_id))

    def remove_project_task(self, task_id, project_id=0):
        if project_id:
            self.load_project(project_id)
        if not self.project:
            self.logger.error('Cannot remove task: there is no loaded project.')
            raise AttributeError('Project is not loaded')
        self.project.remove_task(task_id)
        self.logger.info('Task #{id} removed'.format(id=task_id))

    def complete_project_task(self, task_id, project_id=0):
        if project_id:
            self.load_project(project_id)
        if not self.project:
            self.logger.error('Cannot resolve task: there is no loaded project.')
            raise AttributeError('Project is not loaded')
        self.project.resolve_task(task_id, self.current_dev.login)
        self.logger.info('Task #{id} resolved'.format(id=task_id))

    def move_project_task(self, source_id, destination_id, project_id=0):
        if project_id:
            self.load_project(project_id)
        if not self.project:
            self.logger.error('Cannot move task: there is no loaded project.')
            raise AttributeError('Project is not loaded')
        self.project.move_task(source_id, destination_id)
        self.logger.info('Task #{source_id} moved to sub tasks of #{dest}'.format(source_id=source_id,
                                                                                  dest=destination_id))

    def get_projects(self):
        folder = os.path.join(self.database_folder, 'projects')
        projects = []
        if not os.path.exists(self.database_folder):
            os.mkdir(self.database_folder)
            return ''
        elif os.path.exists(folder):
            files = [f for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f))]
            for file_name in files:
                if file_name == 'last_project.json':
                    continue
                with open(os.path.join(folder, file_name), 'r') as f:
                    try:
                        project = jsonpickle.decode(f.readline())
                        projects.append(str(project))
                    except:
                        continue
        return projects

    def get_project_users(self, project_id=0):
        if project_id:
            self.load_project(project_id)
        if not self.project:
            raise AttributeError('Project is not loaded')
        developers = []
        removed_users = []
        for user_id in self.project.developers:
            if user_id in self.developers:
                developers.append(str(self.developers[user_id]))
            else:
                removed_users.append(user_id)
        for user_id in removed_users:
            self.project.developers.remove(user_id)
        return developers

    def sort_user_tasks(self, sort_type):
        if not self.current_dev:
            raise AttributeError('You are not logged in')
        sorts = {
            'name': lambda task: task.name,
            'deadline': lambda task: task.date,
            'priority': lambda task: task.priority}
        if sort_type in sorts:
            self.current_dev.pending_tasks.sort_by(sorts[sort_type])
            self.logger.info('Tasks sorted by ' + str(sort_type))
        else:
            self.logger.error('Cannot sort with parameter ' + str(sort_type))
            raise KeyError('No such sort type')

    def sort_project_tasks(self, sort_type, project_id=0):
        if project_id:
            self.load_project(project_id)
        if not self.project:
            raise AttributeError('Project is not loaded')
        sorts = {
            'name': lambda task: task.name,
            'deadline': lambda task: task.date if task.date else datetime.datetime(year=2900, month=1, day=5),
            'priority': lambda task: task.priority
        }
        if sort_type in sorts:
            self.project.pending_tasks.sort_by(sorts[sort_type])
            self.logger.info('Tasks sorted by ' + str(sort_type))
        else:
            self.logger.error('Cannot sort with parameter ' + str(sort_type))
            raise KeyError('No such sort type')