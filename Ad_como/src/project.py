import hashlib

from .task_list import TaskList

class Project():
    def __init__(self, name='Simple project'):
        self.name = name
        self.id = 0
        self.to_do_tasks = TaskList({})
        self.in_progress_tasks = TaskList({})
        self.resolved_tasks = TaskList({})
        self.developers = set()
        self.id = hashlib.sha224(bytes(str(self), 'utf-8')).hexdigest()[:10]

    def __str__(self):
        return '{0} {1}\nTo do:\n{2}\nResolved:\n{3}\nDevelopers:\n{4}'.format(self.id, self.name, self.to_do_tasks,
                                                                      self.resolved_tasks, self.developers)

    def add_developer(self, developer_login):
        if developer_login not in self.developers:
            self.developers.add(developer_login)
        else:
            raise Exception('Developer with that login has already exist')

    def add_task(self, task):
        if task.status == 'To do':
            self.to_do_tasks.add_task(task)
        elif task.status == 'In progress':
            self.in_progress_tasks.add_task(task)
        elif task.status == 'Resolved':
            self.resolved_tasks.add_task(task)

    def change_task(self, task_id, name=None, subject=None, description=None, priority=None, assignee=None, dead_line=None):
        if task_id not in self.to_do_tasks.tasks:
            raise KeyError('Task with current id does not exist')
        self.to_do_tasks.tasks[task_id].change(name=name, description=description, subject=subject, assignee=assignee,
                                               priority=priority, deadline=dead_line)





