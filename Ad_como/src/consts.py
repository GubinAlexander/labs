STATUSES_DB = tuple(enumerate([
    'To do',
    'In progress',
    'Closed',
    'Resolved'
]))

DEVELOPERS_DB = tuple(enumerate([
    'Alex',
    'Dmitriy',
    'Kirill',
    'Andrei'
]))

PRIORITIES_DB = tuple(enumerate([
    'Normal',
    'High',
    'Low'
]))

TIME_REPEAT_DB = tuple(enumerate([
    'Day',
    'Week',
    'Month'
]))

TIME_REPEAT = ('Day', 'Week', 'Month')

FOLDER_DEFAULT_NAME = 'Data'

DEFAULT_LOG_FILE_NAME = 'Log_file'

DEVELOPERS = {y: y for x, y in DEVELOPERS_DB}
STATUSES = {y: y for x, y in STATUSES_DB}
PRIORITIES = {y: y for x, y in PRIORITIES_DB}
REPEAT = {y: y for x, y in TIME_REPEAT_DB}

TASK_DEFAULT_PRIORITY = PRIORITIES['Normal']
TASK_DEFAULT_STATUS = STATUSES['To do']
TASK_DEFAULT_DEVELOPER = DEVELOPERS['Alex']
TASK_RESOLVED = STATUSES['Resolved']