from datetime import datetime
import random

from src.task_list import TaskList
from src.consts import TASK_RESOLVED

class Developer():
    def __init__(self, user_name='AlexGubin', name='Alex', password='Alex'):
        self.id = random.randint(0, 10000)
        self.registration_date = datetime.now()
        self.user_name = user_name
        self.password = password
        self.name = name
        self.in_progress_tasks = TaskList({})
        self.to_do_tasks = TaskList({})
        self.resolved_tasks = TaskList({})
        self.all_tasks = TaskList({})
        self.projects = []

    def __str__(self):
        return '{0} {1}'.format(self.name, self.user_name)

    def change_password(self, old_password, new_password):
        if old_password == self.password:
            self.password = new_password
        else:
            raise Exception ('Invalid password')

    def check_password(self, password):
        return self.password == password

    def change_user_name(self, old_user_name, new_user_name=None):
        if old_user_name == self.user_name:
            self.user_name = new_user_name
        else:
            raise Exception('Invalid user name')

    def change_task(self, task_id, name=None, description=None, priority=None, assignee=None,
                    subject=None, dead_line=None, period=None):
        if task_id in self.to_do_tasks.tasks:
            self.to_do_tasks.tasks[task_id].change_task(name, subject, description, priority, assignee, dead_line, period)
        elif task_id in self.in_progress_tasks.tasks:
            self.in_progress_tasks.tasks[task_id].change_task(name, subject, description, priority, assignee, dead_line, period)
        elif task_id in self.resolved_tasks.tasks:
            self.resolved_tasks.tasks[task_id].change_task(name, subject, description, priority, assignee, dead_line, period)
        else:
            raise ValueError('There is no such task id')

    def add_task(self, task):
        if task.status == 'To do':
            self.to_do_tasks.add_task(task)
        elif task.status == 'In progress':
            self.in_progress_tasks.add_task(task)
        elif task.status == 'Resolved':
            self.resolved_tasks.add_task(task)

    def resolve_task(self, task_id):
        if task_id in self.in_progress_tasks.tasks:
            res_task = self.in_progress_tasks.get_task(task_id)
            res_task.status = TASK_RESOLVED
            self.resolved_tasks.add_task(res_task)
            self.to_do_tasks.remove_task(task_id)
        elif task_id in self.to_do_tasks.tasks:
            res_task = self.to_do_tasks.get_task(task_id)
            res_task.status = TASK_RESOLVED
            self.resolved_tasks.add_task(res_task)
            self.to_do_tasks.remove_task(task_id)
        else:
            raise KeyError('There is no task with that id')

    def remove_task(self, task_id):
        if task_id in self.to_do_tasks.tasks:
            self.to_do_tasks.remove_task(task_id)
        elif task_id in self.in_progress_tasks.tasks:
            self.in_progress_tasks.remove_task(task_id)
        elif task_id in self.resolved_tasks.tasks:
            self.resolved_tasks.remove_task(task_id)
        else:
            raise KeyError('There is no such task id')