from setuptools import setup, find_packages

setup(
    name='Ad_como',
    version='1.0',
    packages=find_packages(),
    long_description=open('STATUS.md', 'r').read(),
)