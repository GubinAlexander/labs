import unittest
import random
from src import Task, TaskList


class TestTaskMethods(unittest.TestCase):

    def test_create_task(self):
        task = Task()
        self.assertNotEquals(task.id, '')

    def test_numeration(self):
        size = 10000
        ids = set()
        for i in range(size):
            task = Task(name=str(random.randint(0, 100000)), description=str(random.random()))
            ids.add(task.id)
        self.assertEquals(len(ids), size)

if __name__ == '__main__':
    unittest.main()