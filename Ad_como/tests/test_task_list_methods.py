from src.task_list import TaskList
from src.task import Task

import unittest

class TestTaskListMethods(unittest.TestCase):

    def test_create(self):
        tasks = TaskList()
        self.assertIsInstance(tasks.tasks, dict)

    def test_add_task(self):
        tasks = TaskList()
        task = Task(description='ssfsddf')
        tasks.add_task(task)
        self.assertEquals(len(tasks.tasks), 1)
        self.assertIs(tasks.tasks[task.id], task)

    def test_task_change(self):
        tasks = TaskList()
        task = Task(description='ssfdasddsdaf')
        tasks.add_task(task)
        tasks.tasks[task.id].description = 'darova'
        self.assertEquals(task.description, 'darova')

if __name__ == '__main__':
    unittest.main()